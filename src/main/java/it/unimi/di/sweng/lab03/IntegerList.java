package it.unimi.di.sweng.lab03;

import java.util.function.IntPredicate;

public class IntegerList {
	
	private integerNode head;
	
	public IntegerList(){
		head=null;
	}
	
	public IntegerList(String string) {
		int temp;
		head=null;
		String[] token= string.split(" ");
		for(int i =token.length-1 ; i>=0; i-- ){
			
			temp= Integer.parseInt(token[i]);
			addLast(temp);
			
			
		}
		
	}

	public String toString(){
		
		String result= "[";
		integerNode currentNode=head;
		while(currentNode != null){
			
			result += currentNode.getPrimo();
			
			if(currentNode.getSecondo()!=null){
				result = result+" ";
				}
			currentNode=currentNode.getSecondo();
			
			
			}
		
		result += "]";
		
		return result;
		
		
		
		
	}

	public void addLast(int i) {
		
		integerNode prossimoNodo = new integerNode(i);
		prossimoNodo.setSecondo(head);
		head=prossimoNodo;
	}

}
