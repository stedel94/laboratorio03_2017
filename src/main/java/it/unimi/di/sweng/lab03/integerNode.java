package it.unimi.di.sweng.lab03;

public class integerNode {
	
	private int primo;
	private integerNode secondo= null;

	public integerNode(int i){
		
		primo=i;	
	}
	
	public int getPrimo(){
		return primo;
		
	}
	
	public integerNode getSecondo(){
		
		return secondo;
		
	}
	
	public void setSecondo(integerNode n){
		
		secondo=n;
	}
	
}
